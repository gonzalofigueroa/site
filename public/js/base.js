var url_api = "http://localhost.api_innoapsion.cl";
$(function() {
    llamarApi();

    $("#btn_actualizar").on('click', function(){
        $("#loader").show();
        insertarApi();
    });
});

function llamarApi(){
    $.getJSON(url_api+"/api/bitcoin", {
    }).done(function(data) {
        $("#valor_actual").html(data.peso);
        $("#fecha_valor").html("fecha valor: "+data.fecha);
        $("#loader").hide();
    });

    $.getJSON(url_api+"/api/last_bitcoin", {
    }).done(function(data) {
        $("#ultimo").html(data[0].peso+' - '+data[0].fecha);
        $("#penultimo").html(data[1].peso+' - '+data[1].fecha);
    });

}

function insertarApi(){
    $.getJSON(url_api+"/api/save_bitcoin", {
    }).done(function(data) {
        llamarApi();
        console.log(data);
    });
}